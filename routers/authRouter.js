const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();




const { asyncWrapper } = require('./helpers');
const { validateRegistration } = require('./middlewares/validationMiddleware');
const { login, registration } = require('../controllers/authController');
const {
    authMiddleware
} = require('./middlewares/authMiddleware');



router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

//router.get('/notes', authMiddleware);
router.get('/notes', )

module.exports = router;