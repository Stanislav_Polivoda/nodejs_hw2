const express = require('express');
const router = express.Router();
const  authMiddleware  = require('./middlewares/authMiddleware');
const { Note } = require('../models/noteModel');

router.post('/', authMiddleware, async(req, res) => {
    try {
        const text = req.body.text;

        if (typeof text !== 'string') {
            res.status(400).json({ message: 'text of note must have a string type' });
        }

        const note = new Note({
            userId: req.user._id,
            text: req.body.text,
        });

        await note.save();
        res.status(200).json({ message: 'Success' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/', authMiddleware, async(req, res) => {
    try {
        await req.user.populate({
            path: 'notes',
            options: {
                skip: parseInt(req.query.offset),
                limit: parseInt(req.query.limit),
            },
        }).depopulate('__v').execPopulate();
        res.status(200).json({ notes: [...req.user.notes] });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/:id', authMiddleware, async(req, res) => {
    try {
        const _id = req.params.id;
        const note = await Note.findOne({ _id, userId: req.user._id });

        if (!note) {
            res.status(400).json({ message: 'No task' });
        }
        res.status(200).send(note);
    } catch (error) {
        res.status(400).json({ message: 'No task with this id' });
    }
});

router.put('/:id', authMiddleware, async(req, res) => {
    try {
        const _id = req.params.id;
        const note = await Note.findOne({ _id, userId: req.user._id });

        if (!note) {
            res.status(400).json({ message: 'No task' });
        }

        if (!req.body.text) {
            res.status(400).json({ message: 'Text field can bot be empty' });
        } else {
            if (note.text === req.body.text) {
                res.status(400)
                    .json({ message: 'Please write a new description of note' });
            } else {
                note.text = req.body.text;
                await note.save();
                res.status(200).json({ message: 'Success' });
            }
        }
    } catch (error) {
        res.status(400).json({ message: 'No task with this id' });
    }
});


router.delete('/:id', authMiddleware, async(req, res) => {
    try {
        const _id = req.params.id;
        const note = await Note.findOne({ _id, userId: req.user._id });
        if (!note) {
            res.status(400).json({ message: 'No task' });
        }
        await Note.findOneAndDelete({
            _id,
            userId: req.user._id,
        });
        res.status(200).json({ message: 'Success' });
    } catch (error) {
        res.status(400).json({ message: 'No task with this id' });
    }
});

router.patch('/:id', authMiddleware, async(req, res) => {
    try {
        const _id = req.params.id;
        const note = await Note.findOne({ _id, userId: req.user._id });

        if (!note) {
            res.status(400).json({ message: 'No task' });
        }

        note.completed = !note.completed;
        await note.save();
        res.status(200).json({ message: 'Success' });
    } catch (error) {
        res.status(400).json({ message: 'No task with this id' });
    }
});

module.exports = router;
