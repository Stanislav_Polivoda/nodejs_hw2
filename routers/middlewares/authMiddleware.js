const jwt = require('jsonwebtoken');
const { User } = require("../../models/userModel");
require('dotenv').config();
const { JWT_SECRET } = require('../../config');

const authMiddleware = async(req, res, next) => {
    const header = req.headers['authorization'];

    if (!header) {
        return res.status(400)
            .json({ message: `No Authorization http header found!` });
    }

    const [tokenType, jwtToken] = header.split(' ');
    req.userInfo.tokenType = tokenType;

    await jwt.verify(jwtToken, JWT_SECRET, (err, data) => {
        if (err) {
            return res.status(400).json({ message: 'No valid JWT token found!' });
        }

        req.userInfo = data;
    });

    const _id = req.userInfo._id;
    req.user = await User.findById(_id);
    next();
};

module.exports = authMiddleware;
